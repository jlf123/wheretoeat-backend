var http = require('http'),
    config = require('../config/config.js'),
    request = require('request')

exports.getMenu = function(req, res, next){

    var data = {
        method: 'GET',
        uri: config.zomato_base_uri + 'dailymenu',
        headers: {
            'Content-Type': 'application/json',
            'user-key': config.zomatoApi
        },
        qs: {
            res_id: req.query.res_id
        }
    }

    request.get(data, function(error, response){
        if(!response){
            next();
        } else {
            req.reviews = JSON.parse(response.body).user_reviews
            next();
        }
    })
}