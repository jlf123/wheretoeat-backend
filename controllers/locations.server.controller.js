var http = require('http'),
    config = require('../config/config.js'),
    request = require('request')

exports.getLocations = function(req, res, next){
    var data = {
        method: 'GET',
        uri: config.zomato_base_uri + 'search',
        headers: {
            'Content-Type': 'application/json',
            'user-key': config.zomatoApi
        },
        qs: {
            lat: req.query.lat,
            lon: req.query.lon,
            cuisines: req.query.cuisines,
            radius: req.query.radius
        }
    }

    request.get(data, function(error, response){
        req.locations = JSON.parse(response.body).restaurants
        next();
    })
}