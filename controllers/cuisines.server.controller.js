var http = require('http'),
    config = require('../config/config.js'),
    request = require('request')

exports.getCuisines = function(req, res, next){

    var data = {
        method: 'GET',
        uri: config.zomato_base_uri + 'cuisines',
        headers: {
            'Content-Type': 'application/json',
            'user-key': config.zomatoApi
        },
        qs: {
            lat: req.query.lat,
            lon: req.query.lon
        }
    }
    
    request.get(data, function(error, response){
        req.cuisines = JSON.parse(response.body).cuisines
        next();
    })
    

}
