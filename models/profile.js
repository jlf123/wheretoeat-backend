var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ProfileSchema   = new Schema({
    username: String,
    password: String,
    name: String,
    weight: Number
});

module.exports = mongoose.model('Profile', ProfileSchema);