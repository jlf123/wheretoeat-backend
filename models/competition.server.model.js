/**
 * Created by john.flournoy on 9/7/16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CompetitionSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        trim: true,
        required: 'Title cannot be blank'
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    participants: {
        type: Schema.ObjectId,
        ref: 'User'
    }
    //Add a weigh-in object here that will contain all the weigh-in's
})

mongoose.model('Competition', CompetitionSchema);