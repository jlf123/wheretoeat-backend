var express = require('express'),
    compress = require('compression'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    session = require('express-session'),
    config = require('./config'),
    morgan = require('morgan'),
    flash = require('connect-flash'),
    passport = require('passport')

module.exports = function(){
    var app = express();
    
    if(process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else if(process.env.NODE_ENV === 'production'){
        app.use(compress());
    }

    app.use(bodyParser.urlencoded({
        extended: true
    }))

    app.use(bodyParser.json());

    app.use(methodOverride())

    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: config.sessionSecret
    }))

    app.use(function(req, res, next) {
        // Website you wish to allow to connect
        res.header('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.header('Access-Control-Allow-Credentials', true);

        next();
    });


    app.set('views', './views');
    app.set('view engine', 'ejs');

    app.use(flash());

    app.use(passport.initialize())
    app.use(passport.session())
    
    require('../routes/cuisines.server.routes.js')(app)
    require('../routes/reviews.server.routes.js')(app)
    require('../routes/locations.server.routes.js')(app)
    require('../routes/index.server.routes.js')(app);

    //this needs to go below the routes because if it were above then for http requests it would look through
    //the static files first which would be a lot slower.
    app.use(express.static('./public'));

    return app;
}

/**
 * Note this is the file where we configure our Express application. this is where we add everything related to the Express configuragion
 */
