module.exports = function(app){
    var locations = require('../controllers/locations.server.controller.js')
    
    app.route('/r2t/v1/locations').get(locations.getLocations, function(req, res){
        if(req.locations){
            res.status(200).send(req.locations)
        } else {
            res.status(500).json({
                message: 'Something went wrong'
            })
        }
    })
}