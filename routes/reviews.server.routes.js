module.exports = function(app){
    var reviews = require('../controllers/reviews.server.controller.js');

    app.route('/r2t/v1/reviews').get(reviews.getReviews, function(req, res){
        if(req.reviews){
            res.status(200).send(req.reviews)
        } else {
            res.status(500).json({
                message: 'Something went wrong'
            })
        }
    })

}