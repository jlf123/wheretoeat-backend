/**
 * Created by john.flournoy on 9/27/16.
 */

module.exports = function(app){
    var cuisines = require('../controllers/cuisines.server.controller.js');

    app.route('/r2t/v1/cuisines').get(cuisines.getCuisines, function(req, res){
        if(req.cuisines){
            res.status(200).send(req.cuisines)
        } else {
            res.status(500).json({
                message: 'Something went wrong'
            })
        }
    })

}

